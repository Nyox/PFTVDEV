﻿using UnityEngine;
using System.Collections;

public class PlayerDetector : MonoBehaviour {
    [HideInInspector]
    public bool playerInRange = false;

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag("Player")) {
            playerInRange = true;
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.gameObject.CompareTag("Player")) {
            playerInRange = false;
        }
    }
}
