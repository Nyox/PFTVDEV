﻿using UnityEngine;
using System.Collections;

public class MoveToPoint : MonoBehaviour {

    public GameObject target;
    private UnityEngine.AI.NavMeshAgent navAgent;
    public bool followTarget, followFromStart;
    public bool randomTarget;
    private bool isRandomTargetSet = false;
    private bool isCalculatingPath = false;
    private Vector2 randomPos;
    public float randomPointRadius;

    // Use this for initialization
    void Start() {
        navAgent = gameObject.GetComponent<UnityEngine.AI.NavMeshAgent>();
        target = GameObject.FindGameObjectWithTag("Player");

        if (target && followFromStart) {
            navAgent.SetDestination(target.transform.position);
        }
    }

    void Update() {

        navAgent.nextPosition = transform.position;

        if (target && followTarget && !randomTarget) {
            FaceTarget(target.transform.position);
            navAgent.SetDestination(target.transform.position);
            
        }

        if (randomTarget) {
            if (!isRandomTargetSet && !isCalculatingPath) {
                StartCoroutine(setRandomTarget());
                isCalculatingPath = true;
            }
            else {
                if (Vector3.Distance(transform.position,  (Vector3)randomPos)<1) {
                    isRandomTargetSet = false;
                }
            }
        }

    }

    private void FaceTarget(Vector3 destination) {
        Vector3 lookPos = destination - transform.position;
        lookPos.y = 0;
        Quaternion rotation = Quaternion.LookRotation(lookPos);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 15f);
    }

    private IEnumerator setRandomTarget() {

        randomPos = Random.insideUnitCircle * randomPointRadius;

        navAgent.SetDestination(randomPos);

        //Wait until the path is calculated
        while (navAgent.pathPending) {
            yield return 0;
        }

        //The newPos is not contained into the NavMesh, so try again
        if (navAgent.pathStatus == UnityEngine.AI.NavMeshPathStatus.PathInvalid) {
            StartCoroutine(setRandomTarget());
        }
        //The new position is valid
        else {

            isRandomTargetSet = true;
            isCalculatingPath = false;
        }
    }
}
