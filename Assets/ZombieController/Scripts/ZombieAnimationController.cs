﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

[RequireComponent(typeof(Animator))]
public class ZombieAnimationController : MonoBehaviour {

    [HideInInspector]
    public Animator anim;                           //Character animator controller reference
    private float hSpeed;                           //Animator parameter hSpeed
    private float animationSpeedSmoothing;
    [Range(0.1f,1)]
    public float walkToRunBlending;                // Percentage of the maximun agent speed to blend between walk and run
    NavMeshAgent agent;

    // Use this for initialization
    void Start () {
        //Setting up references
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
    }
	
    public void motionAnimationsHandler(float currentSpeed, float maxSpeed) {
        //idle
        if (currentSpeed <= 0.1) {
            blendMotionAnimations(currentSpeed, 0.5f);
        }
        //walk
        else if (currentSpeed > 0.1 && currentSpeed < maxSpeed * walkToRunBlending) {
            blendMotionAnimations(currentSpeed, 0.5f);
        }
        //run
        else if (currentSpeed >= maxSpeed * walkToRunBlending) {
            blendMotionAnimations(currentSpeed, 0.5f);
        }
    }

    private void blendMotionAnimations(float targetSpeed, float acceleration) {
        hSpeed = Mathf.SmoothDamp(hSpeed, targetSpeed, ref animationSpeedSmoothing, acceleration);
        // Update motion animation parameters
        anim.SetFloat("hSpeed", hSpeed);
    }

    public void triggerAttackAnimation(int attackType = -1) {

        if (attackType == -1) {
            //randomize the attack type
            attackType = Random.Range(0, 7);
        }

        anim.SetFloat("attackType", attackType);
        anim.SetTrigger("attack");
    }

    public void triggerRandomBehaviourAnimation(int behaviourType = -1) {

        if (behaviourType == -1) {
            //randomize the attack type
            behaviourType = Random.Range(0, 3);
        }

        anim.SetFloat("randomBehaviourType", behaviourType);
        anim.SetTrigger("randomBehaviour");
    }

    void OnAnimatorMove() {
        //    // Update position based on animation movement using navigation surface height
        //    Vector3 position = animationController.anim.rootPosition;
        //    position.y = agent.nextPosition.y;
        //    transform.position = position;

        agent.velocity = anim.deltaPosition / Time.deltaTime;
        //transform.rotation = anim.rootRotation;
    }
}
