﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    [System.Serializable]
    public struct Spawn
    {
        public string Name;
        public GameObject spawner;
    }

    [System.Serializable]
    public struct Enemy
    {
        public string Name;
        public GameObject EnemyType;
    }

    public Spawn[] SpawnList;
    public Enemy[] EnemyTypes;
    public float SpawnTimer;
    public int amountPerSpawn = 1;

    private bool isSpawned = false;
    private int amountSpawned = 0;

    void Start()
    {
        if(SpawnList == null || SpawnList.Length == 0)
        {
            SpawnList = new Spawn[4];
            for (int i = 0; i < SpawnList.Length; i++)
            {
                SpawnList[i].spawner = transform.GetChild(i).gameObject;
            }
        }
    }

    void Update()
    {
        spawnEnemy();
    }

    private void setEnemy()
    {
        int spawnerPoint = Random.Range(0, SpawnList.Length);
        int enemySpawn = Random.Range(0, EnemyTypes.Length);
        Instantiate(EnemyTypes[enemySpawn].EnemyType, SpawnList[spawnerPoint].spawner.transform.position, SpawnList[spawnerPoint].spawner.transform.rotation);
        amountSpawned++;
    }

    private IEnumerator timer()
    {
        isSpawned = true;
        for (int i = 0; i < amountPerSpawn; i++)
        {
            setEnemy();
        }
        yield return new WaitForSeconds(SpawnTimer);

        isSpawned = false;
    }

    private void spawnEnemy()
    {
        if (isSpawned == false)
        {
            StartCoroutine(timer());
        }
    }
}
