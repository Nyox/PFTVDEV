﻿using UnityEngine;

public class GunAiming : MonoBehaviour
{
    [System.Serializable]
    public struct GunAimingData
    {
        public Transform gunTransform;
        public float aimingZoom;
        public float hipZoom;
        public Vector3 aimingPos;
        public Vector3 aimingRot;
        public Vector3 hipPos;
        public Vector3 hipRot;
        public float aimingSpeed;

        public GunAimingData(Transform gunTransform, float aimingZoom, float hipZoom, Vector3 aimingPos, Vector3 aimingRot, Vector3 hipPos, Vector3 hipRot, float aimingSpeed)
        {
            this.gunTransform = gunTransform;
            this.aimingZoom = aimingZoom;
            this.hipPos = hipPos;
            this.aimingPos = aimingPos;
            this.aimingRot = aimingRot;
            this.hipZoom = hipZoom;
            this.hipRot = hipRot;
            this.aimingSpeed = aimingSpeed;
        }
    }

    public Camera FPSCamera;
    public GameObject Crosshair;
    public bool lockAiming = false;
    public GunAimingData gunAimingConfig = new GunAimingData(null, 30f, 60f, Vector3.zero, Vector3.zero, Vector3.zero, Vector3.zero, 10f);
    private Vector3 smoother;
    private float Fsmoother;
    public bool isAiming { get; private set; }

    void Update()
    {
        float aimSpeed = gunAimingConfig.aimingSpeed * Time.deltaTime;

        if (isAiming)
        {
            gunAimingConfig.gunTransform.localPosition = Vector3.SmoothDamp(gunAimingConfig.gunTransform.localPosition, gunAimingConfig.aimingPos, ref smoother, aimSpeed);
            gunAimingConfig.gunTransform.localRotation = Quaternion.Euler(gunAimingConfig.aimingRot.x, gunAimingConfig.aimingRot.y, gunAimingConfig.aimingRot.z);
            FPSCamera.fieldOfView = Mathf.SmoothDamp(FPSCamera.fieldOfView, gunAimingConfig.hipZoom, ref Fsmoother, aimSpeed);
            Crosshair.SetActive(false);
        }
        else
        {
            gunAimingConfig.gunTransform.localPosition = Vector3.SmoothDamp(gunAimingConfig.gunTransform.localPosition, gunAimingConfig.hipPos, ref smoother, aimSpeed);
            gunAimingConfig.gunTransform.localRotation = Quaternion.Euler(gunAimingConfig.hipRot.x, gunAimingConfig.hipRot.y, gunAimingConfig.hipRot.z);
            FPSCamera.fieldOfView = Mathf.SmoothDamp(FPSCamera.fieldOfView, gunAimingConfig.hipZoom, ref Fsmoother, aimSpeed);
            Crosshair.SetActive(true);
        }

    }

    public void OnButtonDown()
    {
        isAiming = (lockAiming) ? !isAiming : true;
    }

    public void OnButtonUp()
    {
        if (!lockAiming) isAiming = false;
    }
}
