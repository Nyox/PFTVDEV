﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct GunData
{
    [System.Serializable]
    public struct FireWeaponData
    {
        public FIRETYPE firetype;
        public float power;
        public float recoil;
        public float fireRate;
        public float range;
        public int magazineCapacity;
        [Range(0, 1f)]
        public float muzzleFireFrequency;
        public int currentAmmo { get; set; }

        public FireWeaponData(FIRETYPE firetype, float power, float recoil, float fireRate, float range, int magazineCapacity, float muzzleFireFrequency)
        {
            this.firetype = firetype;
            this.power = power;
            this.recoil = recoil;
            this.fireRate = fireRate;
            this.range = range;
            this.magazineCapacity = magazineCapacity;
            this.muzzleFireFrequency = muzzleFireFrequency;
            currentAmmo = magazineCapacity;
        }
    }
    
}
